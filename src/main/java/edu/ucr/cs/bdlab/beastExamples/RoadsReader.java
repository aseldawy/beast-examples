/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.beastExamples;

import edu.ucr.cs.bdlab.geolite.EnvelopeND;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.FeatureReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateXY;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

import java.io.IOException;

/**
 * Reads the road network dataset provided by SpatialHadoop on the following link
 * https://drive.google.com/file/d/0B1jY75xGiy7ecDEtR1V1X21QVkE
 *
 */
@FeatureReader.Metadata(
    description = "Parses a CSV file that contains line segments",
    shortName = "roadnetwork",
    extension = ".csv"
)
public class RoadsReader extends FeatureReader {
  private static final Log LOG = LogFactory.getLog(RoadsReader.class);

  /**An underlying reader for the text file*/
  protected final LineRecordReader lineReader = new LineRecordReader();

  /**The returned feature*/
  protected CSVFeature feature;

  /**The MBR of the current feature*/
  protected final EnvelopeND featureMBR = new EnvelopeND(2);

  protected GeometryFactory factory = featureMBR.getFactory();

  @Override
  public void initialize(InputSplit split, TaskAttemptContext context) throws IOException {
    lineReader.initialize(split, context);
  }

  @Override
  public boolean nextKeyValue() throws IOException {
    if (!lineReader.nextKeyValue())
      return false;
    Text value = lineReader.getCurrentValue();
    try {
      double x1 = Double.parseDouble(CSVFeature.deleteAttribute(value, ',', 2, CSVFeatureReader.DefaultQuoteCharacters));
      double y1 = Double.parseDouble(CSVFeature.deleteAttribute(value, ',', 2, CSVFeatureReader.DefaultQuoteCharacters));
      double x2 = Double.parseDouble(CSVFeature.deleteAttribute(value, ',', 3, CSVFeatureReader.DefaultQuoteCharacters));
      double y2 = Double.parseDouble(CSVFeature.deleteAttribute(value, ',', 3, CSVFeatureReader.DefaultQuoteCharacters));
      Coordinate[] coordinates = {
              new CoordinateXY(x1, y1),
              new CoordinateXY(x2, y2)
      };
      LineString lineString = factory.createLineString(coordinates);
      feature = new CSVFeature(lineString);
      feature.setFieldSeparator((byte) ',');
      feature.setFieldValues(value.toString());
      featureMBR.setEmpty();
      featureMBR.merge(feature.getGeometry());
      return true;
    } catch (Exception e) {
      LOG.error(String.format("Error reading object at %d with value '%s'", lineReader.getCurrentKey().get(), value));
      throw e;
    }
  }

  @Override
  public EnvelopeND getCurrentKey() {
    return featureMBR;
  }

  @Override
  public IFeature getCurrentValue() {
    return feature;
  }

  @Override
  public float getProgress() throws IOException {
    return lineReader.getProgress();
  }

  @Override
  public void close() throws IOException {
    lineReader.close();
  }
}
